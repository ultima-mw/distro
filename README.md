# Konfiguracja
Instrukcja przygotowana została pod Linuxa (np. Ubuntu), również powinna działać na MacOS, ponieważ większość graczy posiada jednak windowsa, dodałem taki rozdział - ale niestety jest on słabo przetestowany i wymaga więcej manualnej pracy niż na linuxa.

## Linux

### Wymagania
Do uruchomienia i zbudowania projektu potrzebny jest `make` oraz `docker`.

W dockerze znajdują się wszystkie potrzebne narzędzia oraz programy do zbudowania oraz uruchomienia serwera.
`make docker` pozwala pobrać oraz uruchomić obraz, który podmontuje aktualny folder, więc wszystkie rezultaty przetrwają kolejne uruchomienia dockera. Skompilowane pliki `.ecl` oraz przekonwertowane pliki mapy `realms/` będą dostępne na hostcie.

### Pobranie assetów
1. Uruchom obraz dockerowy `make docker`.
2. W konsoli kontenera `make mul` 
    - pobranie zostana paczka `MUL/mul.7z` oraz rozpakowane pliki map w foldrze `MUL/`.

### Budowanie
1. Uruchom obraz dockerowy `make docker`.
2. W konsoli kontenera `make`
    - przekonwertuje pliki klienta na potrzeby pola
    - przekompiluje wszystkie pliki .src
    - przygotuje folder `data/`

## Windows
Należy pobrać repozytorium, w którym aktualnie jesteś. Możesz to zrobić programem `git` bądź na górze strony znajduje się [przycisk do pobrania kodu](https://gitlab.com/ultima-mw/distro/-/archive/master/distro-master.zip).
### Należy pobrać paczki pików:
- ultima-mw/assets> [map.7z](https://gitlab.com/ultima-mw/assets/-/package_files/8674636/download)
- ultima-mw/tools> [pol-core-099.1-Official Release-x64-2018-02-12.zip](https://gitlab.com/ultima-mw/tools/-/raw/26c30ff4989db1400e4b8026c9fbf057a8caaeb5/pol099.1/windows/pol-core-099.1-Official%20Release-x64-2018-02-12.zip)
#### map.7z
Rozpakuj wszystkie pliki do folderu `./MUL/`. Po rozpakowaniu powinien istnieć np. plik w ścieżce: `./MUL/map0.mul`
#### pol091
Rozpakuj paczkę gdzieś z boku:
- przenieś do głównego folderu `./`: `pol.exe` `libmysql.dll` `uoconvert.exe`.
- przenieś do folderu `./scripts/`: `ecompile.exe` (znajduje się on również w folderze `scripts/` w rozpakowanej paczce).
Pozostałe pliki i foldery nie są potrzebne. Paczkę wykorzystujemy tylko i wyłącznie do przekopiowania wyżej wymienionych plików.
### Budowanie mapy
Uruchom skrypt `realmgen.bat`.

### Budowanie skryptów
Uruchom skrypt `compile.bat`.

### Przygotowanie konta admina
Przekopiuj folder `data-template` i nazwij go `data`.
```
robocopy data-template data /E
```

## Klient
Do serwera można połączyć się dowolnym klientem 2d, ale może to wymagać dodatkowej konfiguracji. Poniżej potrzebne kroki, aby połączyć się przy pomocy CrossUO.
1. Pobierz klienta [CrossUO](https://crossuo.com/)
2. Pobierz aktualne pliki map, gumpy, tłumaczenia
- Linux:
    - Uruchom kontener `make docker`
    - Pobierz pliki `make client`
- Windows:
    - pobierz: ultima-mw/assets> [assets.7z](https://gitlab.com/ultima-mw/assets/-/package_files/8674640/download)
    - rozpakuj do `./MUL/`.
    
3. Uruchom CrossUO i ustaw, a na koniec zapisz:
    - `Profile Name: Local distro`
    - `login: admin`
    - `password: admin`
    - `Shard: <custom shard>`
    - `UO Data Path` wskaż na folder `MUL/` (bądź tam gdzie rozpakowałeś pliki klienta)
    - Advance: `Login Server: 127.0.0.1,5001`
    - Advance: `Client Version: 6.0.14.3`
    - Advance: `Type: Automatic`
    - Advance: `Use Cryptographi` pozostaw odznaczone.
4. Uruchom klienta, przed próbą połączenia musisz włączyć serwer.

# Połączenie z serwerem
## Uruchomienie
### Linux
Uruchom dockera `make docker`, wpisz `pol`.
### Windows
Uruchom konsolę i wpisz `pol`. Można oczywiście również po prostu uruchomić `pol.exe` przez dwuklik, ale jeżeli pojawią sie jakieś problemy, to okno konsoli natychmiast się zamknie i nie będzie można przenalizować co poszło nie tak.

## Pierwsze kroki
Aktualnie serwer ma wiele błędów i proces uruchamiania jest utrudniony, wykonaj dokładnie kroki.

Folder `data` zawiera pusty serwer z jednym kontem `admin/admin` bez żadnej postaci. Po zalogowaniu się na serwer należy utworzyć postać, przez `Advanced` (nie wybieraj `Mage` `Warrior` itd.), wybrać podstawowe skille: `anatomia`, `alchemia`, `druciarstwo` (inne skille mogą spowodować błąd - w szczególności `bushido`). Po stworzeniu postaci ujrzysz na chwilę ekran gry oraz zostaniesz rozłączony. Zaloguj się jeszcze raz. 

W tym momencie zobaczysz postać na zielonym terenie. Postać jest zamrożona. Aby się odmrozić należy ustawić sobie admina `.setadmin` i odmrozić się komendą dostępna dla GMa `.thawall`. Świat jest pusty, więc wszystko trzeba sobie stworzyć np. konia `.npcs d kon`, a następnie go oswoić `.tame`. Całą resztę trzeba odkryć na własną rękę bądź rozbudować to krótkie readme.


# Troubleshooting
1. Obserwuj konsole pola. Zobaczysz tam różnego rodzaju błędy, gdy nie będziesz mógł się połączyć, bądź klient nagle się rozłączy. Jeśli nie możesz w ogole się połączyć z serwerem, to obserwuj konsolę - przy próbie połączenia powinna się pojawić znacząca informacja w konsoli.
2. Wyłączanie pola `ctrl+c`.

